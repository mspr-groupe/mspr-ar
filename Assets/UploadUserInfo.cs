using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class UploadUserInfo : MonoBehaviour
{
    void Start()
    {
        string fileContent = ReadFile();
        if (fileContent != "nofile" && fileContent != "done")
        {
            Upload(fileContent);
        }

    }
    public GameObject Panel;
    public Button OkBtn;
    public Button CancelBtn;
    public InputField emailInput;
    public InputField prenomInput;
    public Text errorText;
    public Image pictureHolder;
    private string URL = "https://api.sendinblue.com/v3/contacts";
    private string path = "Assets/Resources/userInfo.json";

    public void UploadForm()
    {
        string data = "{\"email\": \"" + emailInput.text + "\", \"attributes\":{\"prenom\":\"" + prenomInput.text + "\"}}";
        Upload(data);
    }

    public async void Upload(string data)
    {
        byte[] bodyRaw = Encoding.UTF8.GetBytes(data);
        using (UnityWebRequest www = UnityWebRequest.Put(URL, bodyRaw))
        {
            www.method = "POST";
            www.SetRequestHeader("Content-Type", "application/json");
            // ToDo: Clé à anonymiser
            www.SetRequestHeader("api-key", "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");
            var operation = www.SendWebRequest();
            while (!operation.isDone)
                // On attend le retour de l'API
                await Task.Yield();
            if (www.result == UnityWebRequest.Result.Success)
            {
                Debug.Log($"success: {www.downloadHandler.text}");
                WriteString("done");
                if (emailInput.IsActive())
                {
                    StartCoroutine(TakeScreenshotAndShare());
                    Panel.gameObject.SetActive(false);
                }
                else
                {
                    Debug.Log("SHADOW SEND");
                }
            }
            else
            {
                if (www.responseCode != 400)
                    // On stock les data user pour retenter plus tard
                    WriteString(data);
                else
                    errorText.text = "L'email est invalide";
                Debug.Log($"error: {www.error}");
                // 
            }
        }
    }

    private string TakeScreenshot(){
        Texture2D screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		screenshot.Apply();

		string filePath = Path.Combine(Application.temporaryCachePath, "screenshot.png");
		File.WriteAllBytes(filePath, screenshot.EncodeToPNG());

		Destroy(screenshot);

		return filePath;
    }

    private IEnumerator TakeScreenshotAndShare()
	{
        Debug.Log($"routine appelée");
		yield return new WaitForEndOfFrame();

		string filePath = TakeScreenshot();

		new NativeShare().AddFile(filePath)
			.SetSubject("Partage de mon coloriage #").SetText("#cerealis #coloring #ar").SetUrl("https://cerealis.com")
			.SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
			.Share();

		// Share on WhatsApp only, if installed (Android only)
		//if( NativeShare.TargetExists( "com.whatsapp" ) )
		//	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();
	}

    public void Cancel()
    {    // Quand on appuie sur le bouton "annuler", on vide les champs
        emailInput.text = "";
        prenomInput.text = "";
    }

    public void WriteString(string data)
    {
        Debug.Log($"Write file");
        //Write some text to the file
        StreamWriter writer = new StreamWriter(path, false);
        writer.Write(data);
        writer.Close();
        Debug.Log($"Write success");
    }

    private string ReadFile()
    {
        Debug.Log("On lit le fichier");
        //Read the text from directly from the test.txt file
        try
        {
            StreamReader reader = new StreamReader(path);
            string filecontent = reader.ReadToEnd();
            Debug.Log(filecontent);
            reader.Close();
            return filecontent;
        }
        catch (FileNotFoundException e)
        {
            Debug.Log("Pas de fichier trouvé");
        }
        return "nofile";
    }


}
